package com.s.vup.myapplication;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ChildViewHolder;

public class ChildFoodHolder extends ChildViewHolder {

    private TextView tvCompositionFoodHolder;
    private TextView tvWeightCompositionFoodHolder;

    public ChildFoodHolder(View itemView) {
        super(itemView);
        tvCompositionFoodHolder = itemView.findViewById(R.id.item_child_tv_title);
        tvWeightCompositionFoodHolder = itemView.findViewById(R.id.item_child_tv_weight);
    }

    public void bind(ItemChildFood ItemChildFood) {
        tvCompositionFoodHolder.setText(ItemChildFood.getTitle());
        tvWeightCompositionFoodHolder.setText(ItemChildFood.getWeight() + " g");
    }
}
