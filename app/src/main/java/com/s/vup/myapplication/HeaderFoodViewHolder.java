package com.s.vup.myapplication;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ParentViewHolder;

public class HeaderFoodViewHolder extends ParentViewHolder {

    private TextView tvTitleDescriptionFood;
    private TextView tvWeightDescriptionFood;

    public HeaderFoodViewHolder(View itemView) {
        super(itemView);
        tvTitleDescriptionFood = itemView.findViewById(R.id.item_header_tv_header_title);
        tvWeightDescriptionFood = itemView.findViewById(R.id.item_header_tv_total);
    }

    public void bind(ItemHeaderFood itemHeaderFood) {
        tvTitleDescriptionFood.setText(itemHeaderFood.getTitle());
        tvWeightDescriptionFood.setText(itemHeaderFood.getWeight()+" g");
    }

    @Override
    protected void expandView() {
        super.expandView();
    }

    @Override
    protected void collapseView() {
        super.collapseView();
    }
}