package com.s.vup.myapplication;

import java.io.Serializable;

public class ItemChildFood implements Serializable {
    private String title;

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    private double weight;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ItemChildFood(String title, double weight) {
        this.title = title;
        this.weight = weight;
    }
}