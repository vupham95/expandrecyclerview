package com.s.vup.myapplication;

import com.bignerdranch.expandablerecyclerview.model.Parent;

import java.util.List;

public class ItemHeaderFood implements Parent<ItemChildFood> {
    private String title;
    private double weight;


    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private List<ItemChildFood> mItemChildFoods;

    public ItemHeaderFood(String title, List<ItemChildFood> itemChildFoods, double weight) {
        mItemChildFoods = itemChildFoods;
        this.title = title;
        this.weight = weight;
    }

    @Override
    public List<ItemChildFood> getChildList() {
        return mItemChildFoods;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}