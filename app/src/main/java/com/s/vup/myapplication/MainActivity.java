package com.s.vup.myapplication;

import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    RecyclerView exListNutritionFacts;
    NestedScrollView nestedScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        exListNutritionFacts = (RecyclerView) findViewById(R.id.food_entry_details_lv_exp);
        nestedScrollView = (NestedScrollView) findViewById(R.id.food_entry_details_nested_scroll_parent);
        fakeData();
    }

    private void fakeData() {
        ItemChildFood beef = new ItemChildFood("Dietary Fiber ", 1.2);
        ItemChildFood cheese = new ItemChildFood("Sugar", 0.6);
        ItemChildFood salsa = new ItemChildFood("Saturated Fat", 3.1);
        ItemChildFood tortilla = new ItemChildFood("Trans Fat", 0);

        ItemHeaderFood totalCarbohydrates = new ItemHeaderFood("Total Carbohydrates", Arrays.asList(beef, cheese, salsa, tortilla), 93.3);
        ItemHeaderFood totalFats = new ItemHeaderFood("Total Fats", Arrays.asList(cheese, tortilla, cheese, tortilla), 29.8);
        ItemHeaderFood protein = new ItemHeaderFood("Protein", Arrays.asList(cheese, tortilla, cheese, tortilla, cheese, tortilla), 23.8);
        List<ItemHeaderFood> itemHeaderFoods = Arrays.asList(totalCarbohydrates, totalFats, protein, totalCarbohydrates, totalFats, protein, totalCarbohydrates, totalFats, protein);


        FoodEntryDetailsAdapter adapter = new FoodEntryDetailsAdapter(MainActivity.this, itemHeaderFoods);
        adapter.setExpandCollapseListener(new ExpandableRecyclerAdapter.ExpandCollapseListener() {
            @Override
            public void onParentExpanded(final int parentPosition) {
//                android.os.Handler handler = new android.os.Handler();
//                handler.post(() -> {
////                    nestedScrollView.setScrollY(nestedScrollView.getHeight()+ adapter.getChildHeight());
//                });
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //nestedScrollView.getLayoutParams().height = nestedScrollView.getChildAt(0).getHeight();
                        float target = exListNutritionFacts.getTop() + exListNutritionFacts.getChildAt(parentPosition + 1).getY();
                        float tempt = target / nestedScrollView.getChildAt(0).getHeight();
                        int target2 = (int) (tempt * nestedScrollView.getHeight());
                        nestedScrollView.setScrollY(target2);
                    }
                }, 500);

            }

            @Override
            public void onParentCollapsed(int parentPosition) {

            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        if (exListNutritionFacts != null) {
            exListNutritionFacts.setLayoutManager(linearLayoutManager);
            exListNutritionFacts.setNestedScrollingEnabled(false);
            exListNutritionFacts.setAdapter(adapter);
        }


    }

}
