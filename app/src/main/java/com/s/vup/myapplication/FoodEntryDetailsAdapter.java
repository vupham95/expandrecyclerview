package com.s.vup.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.ExpandableRecyclerAdapter;

import java.util.List;

public class FoodEntryDetailsAdapter extends ExpandableRecyclerAdapter<ItemHeaderFood, ItemChildFood, HeaderFoodViewHolder, ChildFoodHolder> {


    private LayoutInflater mInflater;

    public int getChildHeight() {
        return childHeight;
    }

    public void setChildHeight(int childHeight) {
        this.childHeight = childHeight;
    }

    private int childHeight;

    FoodEntryDetailsAdapter(Context context, @NonNull List<ItemHeaderFood> itemHeaderFoodList) {
        super(itemHeaderFoodList);
        mInflater = LayoutInflater.from(context);
    }


    // onCreate ...
    @NonNull
    @Override
    public HeaderFoodViewHolder onCreateParentViewHolder(@NonNull ViewGroup parentViewGroup, int viewType) {
        View recipeView = mInflater.inflate(R.layout.item_header_food_entry_details, parentViewGroup, false);
        return new HeaderFoodViewHolder(recipeView);
    }

    @NonNull
    @Override
    public ChildFoodHolder onCreateChildViewHolder(@NonNull ViewGroup childViewGroup, int viewType) {
        View ingredientView = mInflater.inflate(R.layout.item_child_food_entry_details, childViewGroup, false);

        setChildHeight(childViewGroup.getHeight());
        return new ChildFoodHolder(ingredientView);
    }

    // onBind ...
    @Override
    public void onBindParentViewHolder(@NonNull HeaderFoodViewHolder headerFoodViewHolder, int parentPosition, @NonNull ItemHeaderFood itemHeaderFood) {
        headerFoodViewHolder.bind(itemHeaderFood);
    }

    @Override
    public void onBindChildViewHolder(@NonNull ChildFoodHolder childFoodHolder, int parentPosition, int childPosition, @NonNull ItemChildFood ItemChildFood) {
        childFoodHolder.bind(ItemChildFood);
    }


}